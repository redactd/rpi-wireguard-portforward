## What?

How-to guide to implement port-forwarding using WireGuard, LightSail, RaspAP. 

- https://www.wireguard.com/
- https://aws.amazon.com/lightsail/
- https://raspap.com/


The purpose of this guide is to help you set up a system that will allow you to port forward past [CGNAT](https://en.wikipedia.org/wiki/Carrier-grade_NAT#Disadvantages) environments.

## Why?

Running a server inside of a CGNAT environment prevents you from being able to reach directly into the network from outside. 

If the service you want to reach is running on a device that you can install a VPN client on - you can use a wireguard/openvpn tunnel or a service like portmap.io. These options are well documented and a fairly trivial way to access that server. 

For this scenario I couldn't run the VPN client on the hardware appliance that I needed to expose. Instead I had to move the tunnel and port forwarding up the stack to the device's gateway.

Even if you don't have a server you need to reach insdie your network you can still configure a tunnel like this and choose which devices route across. You could have your Roku appear to be in another location while the rest of your network is unaffected, as an example.

## How?

For the most part I'm just going to be linking to various guides on how to make this all work. I didn't come up with the vast majority of this - I'm just helping you glue it all together.

- Install RaspiOS on Raspberry Pi 4.
- Install RaspAP on RPi to provide web management of the network and wireless hotspot
- Install and configure WireGuard on RPi
- Create AWS Lightsail instance
- Install and configure WireGuard on Lightsail
- Activate WireGaurd tunnell
- Route traffic and forward ports

## Topology

You can implement this solution in any topology. Here's what a basic diagram looks like for this guide.
![basic topology](img/basic_topology.png "basic")

In my situation I'm sitting on a [Starlink](https://www.starlink.com/) network. Even though the packets are going to space and back the diagram will look essentially the same. Another similar diagram would be a cellular network - just imagine the cell tower is in the same position as the sattelite dish in this diagram.
![starlink topology](img/basic_topology_starlink.png "starlink")

For the rest of this guide, I'll be simplifying the diagram by removing the CGNAT bits. __The CGNAT still exists__ but the image will be condensed.


## Required Hardware
```
If you need wireless, Raspberry Pi 4 2GB+
If you just need ethernet, any RPi or Debian based device will technically work. 
```

## Install and Configure OS on RPi

If you need help installing an OS, here's a [guide](https://www.pragmaticlinux.com/2020/11/perform-a-minimal-install-on-your-raspberry-pi/)

You need to just install the minimal/basic install. Basic steps: 

* Flash OS image to SD Card with balenaEtcher
* Enable SSH before by creating a blank file called "ssh" in the root of the newly created SD card
* Install SD card and boot the Pi
* Plug the Pi into an ethernet port and figure out what IP was assigned
* SSH pi/raspberry into the Pi

## Initial Setup
```
user@raspberrypi:>
sudo -s
apt-get update
apt-get full-upgrade -y
raspi-config
#set wlan country. reboot.
```

## Install [RaspAP](https://raspap.com/#quick)

The Insiders edition of RaspAP has support for WireGaurd built in. This won't be necessary for this guide but I recommend supporting projects that make things like this possible.

https://docs.raspap.com/insiders/


You'll be taking the defaults during the install
```
user@raspberrypi:>
sudo -s
curl -sL https://install.raspap.com | bash
```

## Create Lightsail instance 
* https://lightsail.aws.amazon.com/ls/webapp/home/instances?#
* Create Instance
* Linux -> OS Only
* Ubuntu 20.04
* make sure your ssh keypair is sorted
* $3.50/month
* name it
* create instance
* Create UDP rule
* ssh into new instance
* Install WireGuard
```
user@lightsail:>
sudo -s
apt-get update
apt-get install wireguard -y
sudo systemctl enable wg-quick@wg0
```
* enable ip forwarding
```
root@lightsail:>
nano /etc/sysctl.conf 
```
* Search for the line #net.ipv4.ip_forward uncomment this line by removing the # at the beginning.
```
root@lightsail:>
sysctl -p
cat /proc/sys/net/ipv4/ip_forward
```

## Install WireGuard on Pi
```
user@raspberrypi:>
sudo apt-get install -y wireguard
sudo systemctl enable wg-quick@wg0
```
* enable ip forwarding
```
root@raspberrypi:>
nano /etc/sysctl.conf 
```
* Search for the line #net.ipv4.ip_forward uncomment this line by removing the # at the beginning.
```
root@raspberrypi:>
sysctl -p
cat /proc/sys/net/ipv4/ip_forward
```


## Generate WireGuard Config
Webiste to make this easier: https://www.wireguardconfig.com/
* random seed. make changes if extra paranoid.
* listen port: 51820
* CIDR: 10.1.0.0/24
* Client Allowed IPs: 0.0.0.0/0, ::/0
* Endpoint: your.lightsail.public.ip:51820
* Use Pre-Shared
* Generate Config

## Create WireGuard config files

### Lightsail
```
root@lightsail:>
nano /etc/wireguard/wg0.conf
```
copy/paste from config generator

```
[Interface]
Address = 10.1.0.1/24
ListenPort = 51820
PrivateKey = *****
PostUp = iptables -A FORWARD -i %i -j ACCEPT; iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE

[Peer]
PublicKey = *****
PresharedKey = *****
AllowedIPs = 10.1.0.2/32
```

### Raspberry Pi
```
root@raspberrypi:>
nano /etc/wireguard/wg0.conf
```
copy/paste from config generator

```
[Interface]
Address = 10.1.0.2/24
ListenPort = 51820
PrivateKey = *****

[Peer]
PublicKey = *****
PresharedKey = *****
AllowedIPs = 0.0.0.0/0, ::/0
Endpoint = public.ip.address:51820
```

### Bring up tunnel

Start the WireGuard tunnel on each system
```
root@raspberrypi:>
wg-quick up wg0

root@lightsail:>
wg-quick up wg0

```

You should now have an active tunnel between the two.
```
root@raspberrypi:>
wg show
```

```
interface: wg0
  public key: 
  private key: (hidden)
  listening port: 51820

peer: 
  preshared key: (hidden)
  endpoint: *.*.*.*:51820
  allowed ips: AllowedIPs = 0.0.0.0/0, ::/0
  **latest handshake: 1 minute, 1 second ago**
  transfer: 92 B received, 57.72 KiB sent
```

On the Lightsail instance you should be able to ping across the tunnel.
```
root@lightsail:>

ping 10.1.0.2

PING 10.1.0.2 (10.1.0.2) 56(84) bytes of data.
64 bytes from 10.1.0.2: icmp_seq=1 ttl=64 time=41.3 ms
64 bytes from 10.1.0.2: icmp_seq=2 ttl=64 time=36.4 ms
64 bytes from 10.1.0.2: icmp_seq=3 ttl=64 time=48.7 ms
64 bytes from 10.1.0.2: icmp_seq=4 ttl=64 time=48.6 ms
```

## Use Case

Now that you have an active WireGuard tunnel between your two endpoint you need to determine your use case in order to proceed.

For this example we are going to assume that you will have your RaspberryPi eth0 plugged into the LAN and receive a DHCP IP. I'll go over two scenarios. The first here the RPi will be an ethernet only VPN gateway and another where you will configure RaspAP to broadcast a WiFi network that hands out an IP in the 10.3.141.x range to its clients and those clients are on the VPN.

It won't be covered here (yet?) but you could have scenarios where the Pi is not connected at all via ethernet but instead the wireless card would act as both a client and server via ([AP-STA](https://docs.raspap.com/ap-sta/))

### Scenario 1 (ethernet only)

The first scenario will be a wired device on the same LAN as your RPi. The client device has IP 192.168.1.120 and we will be running a webserver on port 8000 that we want to expose to the internet.

![wireguard lan topology](img/wireguard_lan_topology.png "wglan")

- Make sure TCP 8000 is allowed on your lightsail firewall
- Make sure your client 192.168.1.120 is using the RPi as it's default gateway!

We need to forward TCP 8000 across the WireGuard tunnel to the RPi. You'll notice that we are landing on port **8001** on the RPi. Since the RPi is not the final destination we need to change the port in order to forward on to the next device.

```
root@lightsail:>
iptables -t nat -I PREROUTING -i eth0 -p tcp -m tcp --dport 8000 -j DNAT --to-destination 10.1.0.2:8001
```

We then need to forward the packets from the RPi on to our client at the desired port.
```
root@raspberrypi:>
iptables -t nat -I PREROUTING -i wg0 -p tcp -m tcp --dport 8001 -j DNAT --to-destination 192.168.1.120:8000
```

In this demo there is a simple HTTP server running on TCP Port 8000 on 192.168.1.120. When a user requests the page from the Lightsail instance http://34.2213.100.8:8000 the request will traverse the WireGuard tunnel and land at the server inside the network.

![wireguard lan request](img/wireguard_lan_tcp_request.png "wglantcp")

### Scenario 2 (Wireless Hotspot)

Same as in Scenario 1 we forward TCP 8000 across the WireGuard tunnel to the RPi on port 8001.

```
root@lightsail:>
iptables -t nat -I PREROUTING -i eth0 -p tcp -m tcp --dport 8000 -j DNAT --to-destination 10.1.0.2:8001
```

We again need to forward the packets to the client that is connected to the Wireless Hotspot on the RPi.
```
root@raspberrypi:>
iptables -t nat -I PREROUTING -i wg0 -p tcp -m tcp --dport 8001 -j DNAT --to-destination 10.3.141.10:8000

```

This scenario is very similar to ethernet only but worth a separate call out. In this case your server is connected to the RPi over the Hotspot network it's broadcasting. The client IP is 10.3.141.10
![wireguard wireless topology](img/wireguard_wireless_request2.png "wgwireless")

### Save IPTables
We need to keep our rules persistant across reboots. [Here's a guide](https://sleeplessbeastie.eu/2018/09/10/how-to-make-iptables-configuration-persistent/) on how to do that. 

```
root@lightsail:>
apt-get install iptables-persistent
```

```
root@raspberrypi:>
apt-get install iptables-persistent
```

## Conclusion

You are done. The web server is reachable over the internet.

It's importatnt to make sure you only open up the ports to the networks that need them within your Lightsail firewall. If your intention is to open http/https to the world, only open that port.

